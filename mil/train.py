import argparse
import datetime
import pprint
import numpy as np
import torch
import torch.utils.data as data_utils
import torch.optim as optim

# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
import seaborn as sns

from tensorboardX import SummaryWriter
from tensorboardX.utils import figure_to_image

from mnistbags import MnistBags
from model import Network, calculate_loss, calculate_error, decide_label

sns.set()  # Set seaborn defaults for plotting

parser = argparse.ArgumentParser(description='PyTorch MNIST bags Example')
parser.add_argument('--epochs', type=int, default=6, metavar='N',
                    help='number of epochs to train (default: 6)')
parser.add_argument('--lr', type=float, default=0.0005, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--reg', type=float, default=10e-5, metavar='R',
                    help='weight decay')
parser.add_argument('--mean_bag_length', type=int, default=10, metavar='ML',
                    help='average bag length')
parser.add_argument('--var_bag_length', type=int, default=2, metavar='VL',
                    help='variance of bag length')
parser.add_argument('--min_bag_length', type=int, default=4, metavar='MNL',
                    help='minimum bag length')
parser.add_argument('--max_bag_length', type=int, default=15, metavar='MXL',
                    help='maximum bag length')
parser.add_argument('--num_bags_train', type=int, default=200, metavar='NTrain',
                    help='number of bags in training set')
parser.add_argument('--num_bags_test', type=int, default=50, metavar='NTest',
                    help='number of bags in test set')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('-n', '--name', default='', help='Experiment name (for tensorboard)')
args = parser.parse_args()

timestamp = datetime.datetime.now().strftime('%H-%M-%S')
# Tensorboard
expname = args.name
tb_logdir = f'/tmp/bbtb/{expname}_{timestamp}'
writer = SummaryWriter(log_dir=tb_logdir)

device = torch.device('cuda') if torch.cuda.is_available()\
    else torch.device('cpu')

torch.manual_seed(args.seed)
loader_kwargs = {'num_workers': 0}  # Additional kwargs for DataLoader initialization
if device.type == 'cuda':
    torch.cuda.manual_seed(args.seed)
    loader_kwargs = {'num_workers': 1, 'pin_memory': True}
    print('\nGPU is ON!')

print('Load Train and Test Set')

# target_classes = [3, 4, 5]
target_classes = list(range(10))

n_classes = len(target_classes)


def _worker_init_fn(worker_id):
    np.random.seed(torch.initial_seed() + worker_id)


loader_kwargs['worker_init_fn'] = _worker_init_fn

train_loader = data_utils.DataLoader(
    MnistBags(
        target_classes=target_classes,
        mean_bag_length=args.mean_bag_length,
        var_bag_length=args.var_bag_length,
        min_bag_length=args.min_bag_length,
        max_bag_length=args.max_bag_length,
        num_bags=args.num_bags_train,
        seed=args.seed,
        train=True,
        # augment=True,
        order='brace',
    ),
    batch_size=1,
    shuffle=True,
    **loader_kwargs
)

test_loader = data_utils.DataLoader(
    MnistBags(
        target_classes=target_classes,
        mean_bag_length=args.mean_bag_length,
        var_bag_length=args.var_bag_length,
        min_bag_length=args.min_bag_length,
        max_bag_length=args.max_bag_length,
        num_bags=args.num_bags_test,
        seed=args.seed,
        train=False,
        # augment=True,  # Apply augmentations also at test time (useful for transformation visualization)
        # order=None,  # Set to fail
        order='brace',
    ),
    batch_size=1,
    shuffle=False,
    **loader_kwargs
)

print('Init Model')
model = Network(n_classes=n_classes, positional=True, att_mode='add').to(device)
# model = Network(n_classes=n_classes, positional=True, att_mode='msdp').to(device)  # Currently broken. TODO

optimizer = optim.Adam(model.parameters(), lr=args.lr, betas=(0.9, 0.999), weight_decay=args.reg)


def visualize_bag_imgs(bag, labels=None):
    fig, axes = plt.subplots(3, 5)#,figsize=(10, 6))
    if labels is None:
        labels = [f'{k}' for k in range(bag.shape[0])]
    bag = bag.detach().squeeze()  # (N, K, C, H, W) -> (K, H, W)
    if bag.shape[0] > 15:
        bag = bag[:15]
        print('Warning: This bag has been shortened to 15 elements for plotting.')
    for k in range(bag.shape[0], 15):
        fig.delaxes(axes.flat[k])
    for k, (img, lab) in enumerate(zip(bag, labels)):
        ax = axes.flat[k]
        ax.imshow(img, cmap='gray')
        ax.set_axis_off()
        ax.set_title(lab)
    return fig


def visualize_attention_grid(attention, target_classes=target_classes):
    fig, ax = plt.subplots()
    attperc = (attention * 100).to(torch.uint8)
    sns.heatmap(attperc, vmin=0, vmax=100, annot=True, cmap='viridis', linewidths=.5, ax=ax)
    ax.set_title('Scaled attention weights (in %)')
    ax.set_yticklabels(target_classes)
    ax.set_xlabel('Index of instance in bag')
    ax.set_ylabel('Class')
    return fig


def visualize_probs(probs):
    fig, ax = plt.subplots()
    probsperc = (probs * 100).to(torch.uint8)
    sns.heatmap(probsperc.unsqueeze(1), vmin=0, vmax=100, cmap='viridis', annot=True, linewidths=.5)
    ax.set_title('Predicted bag label probabilities (in %)')
    ax.set_yticklabels(target_classes)
    ax.set_ylabel('Class')
    return fig


def train(epoch):
    model.train()
    train_loss = 0.
    train_error = 0.
    optimizer.zero_grad()
    for batch_idx, (imgs, labels) in enumerate(train_loader):
        bag_label, _ = labels  # Only bag-level label is available for training
        # Squeeze batch dimension, because it is replaced by the bag dimension
        imgs, bag_label = imgs.squeeze(0).to(device), bag_label.squeeze(0).to(device)
        imgs.requires_grad = True

        loss = 0.
        probs, attention = model(imgs)
        for c in range(n_classes):
            loss += calculate_loss(probs[c], bag_label[c])
        train_loss = loss.item()
        # From now on we don't need correct gradient tracking, so we can convert to full tensors finally:
        probs = torch.tensor(probs)
        predicted_label = decide_label(probs).to(device)  # TODO: decide_label output should immediately be on the the device of probs.
        error = calculate_error(predicted_label, bag_label)
        train_error += error
        # if batch_idx % 4 == 0:  # Accumulate gradients over multiple bags
        loss.backward()
        optimizer.step()

        # Log weights and gradients (only once at the end of each epoch)
        if batch_idx == len(train_loader) - 1:
            for name, param in model.named_parameters():
                writer.add_histogram(name, param, epoch)
                writer.add_histogram(name + '.grad', param.grad, epoch)

        optimizer.zero_grad()

    # calculate loss and error for epoch
    train_loss /= len(train_loader)
    train_error /= len(train_loader)

    writer.add_scalar('loss/train', train_loss, epoch)
    writer.add_scalar('error/train', train_error, epoch)
    print('Epoch: {}, Loss: {:.5f}, Train error: {:.5f}'.format(epoch, train_loss, train_error))


def k_hot_to_labelname_list(hot):
    label_list = hot.nonzero().reshape(-1).tolist()
    labelname_list = [target_classes[label] for label in label_list]
    return labelname_list


def test(epoch=0):
    torch.set_grad_enabled(False)
    model.eval()
    test_loss = 0.
    test_error = 0.
    for batch_idx, (imgs, labels) in enumerate(test_loader):
        bag_label, instance_labels = labels
        imgs, bag_label = imgs.squeeze(0).to(device), bag_label.squeeze(0).to(device)
        instance_labels.squeeze_(0)
        loss = 0.
        probs, attention = model(imgs)
        for c in range(n_classes):
            loss += calculate_loss(probs[c], bag_label[c])
        # After loss calculation we can convert probs and att to full tensors
        probs = torch.tensor(probs)
        _tatt = torch.empty(len(attention), *attention[0].shape)
        for c in range(_tatt.shape[0]):
            _tatt[c] = attention[c]
        attention = _tatt
        # attention now has shape (n_classes, n_instances)
        # Scale each class in attention tensor by its predicted class
        #  probability. probs.view(-1, 1) makes probs broadcastable to
        #  the attention tensor.
        #  The reason for this applying this scaling is that attention on
        #  classes that were not predicted to be in a bag (-> low class prob)
        #  is not meaningful here.
        #
        #  "On which elements do you focus to decide that a certain class is
        #  NOT contained in a bag?" - is a hard-to-interpret question.
        #  The scaled attention tensor answers the more important question:
        #  "On which elements do you focus to decide that a certain class IS
        #  contained in a bag?".
        scaled_attention = probs.view(-1, 1) * attention
        attention = scaled_attention  # TODO: Make this more explicit in names

        predicted_label = decide_label(probs).to(device)
        test_loss += loss.item()
        error = calculate_error(predicted_label, bag_label)
        test_error += error

        if batch_idx == 0:  # Visualize performance on the first test sample
            # TODO: Clean up distinction between label name and label number (index)
            true_bag_label = k_hot_to_labelname_list(bag_label)
            pred_bag_label = k_hot_to_labelname_list(predicted_label)
            bag_level = f'{true_bag_label}\n{pred_bag_label}'

            true_instance_labels = instance_labels.tolist()
            att = [[int(x * 100) for x in row] for row in attention]  # More readable
            instance_level = f'{true_instance_labels}\n{pprint.pformat(att)}'

            test_results = f'\nTrue Bag Labels, Predicted Bag Labels:\n{bag_level}\n' +\
                           f'True Instance Labels, Attention Weights (in %):\n{instance_level}'
            print(test_results)

            # Tensorboard Markdown needs \n\n for each actual line break
            test_results_tb = test_results.replace('\n', '\n\n')
            writer.add_text('test', test_results_tb, epoch)

            fig_bag_imgs = visualize_bag_imgs(imgs)
            fig_scaled_attention = visualize_attention_grid(attention)
            fig_probs = visualize_probs(probs)
            writer.add_figure('bag/images', fig_bag_imgs, epoch)
            writer.add_figure('bag/scaledattention', fig_scaled_attention, epoch)
            writer.add_figure('bag/probs', fig_probs, epoch)

            # Re-compute embeddings and visualize them in projector
            emb = model.embedding(imgs)
            writer.add_embedding(
                emb, metadata=true_instance_labels, label_img=imgs,
                global_step=epoch, tag='bag')

            # Plot images with superimposed scaled attentions
            # pia = _instance_attentions(attention)
            # fig_bag_imgs = visualize_bag_imgs(imgs, pia)
            # writer.add_figure('bag/images_attlabels', fig_bag_imgs, batch_idx)

    test_error /= len(test_loader)
    test_loss /= len(test_loader)

    writer.add_scalar('loss/test', test_loss, epoch)
    writer.add_scalar('error/test', test_error, epoch)
    print('\nTest Set, Loss: {:.5f}, Test error: {:.5f}\n'.format(test_loss, test_error))


    if epoch == 1:  # Only do this once per run
        # Craft a dummy bag with 5 instances
        dummy_input = torch.empty(5, 1, 28, 28)
        writer.add_graph(model, dummy_input)

    torch.set_grad_enabled(True)


def _fmt(val):
    # formatted = round(val, 2)  # 0.xx
    # formatted = str(int(100 * round(val, 0))) + '%'
    formatted = int(round(100 * val))
    return formatted


# TODO: Cutoff should perhaps scale with bag size (attention.shape[0])
def _instance_attentions(attention, cutoff=0.1):
    # Now each row contatransposedattins class-attentions of one instance
    attention_transp = attention.transpose(0, 1)
    pia = []  # per-instance-attention map
    for inst in attention_transp:
        # Within one instance: Maps class name to the rounded attentions w.r.t. it
        class_attentions = {
            target_classes[c]: _fmt(inst[c].item())
            for c in range(n_classes)
            if inst[c].item() > cutoff
        }
        pia.append(class_attentions)
    return pia


if __name__ == "__main__":
    for epoch in range(1, args.epochs + 1):
        print('Start Training')
        train(epoch)
        print('Start Testing')
        test(epoch)
