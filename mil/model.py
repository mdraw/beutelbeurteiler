import math

import torch
import torch.nn as nn
import torch.nn.functional as F

# import onmt.modules as om

# L: Size of image embedding vectors (latent representation)
# D: Size of hidden layer in Attention modules


######## Multihead Attention stuff from onmt ##########################

# def aeq(*args):
#     """
#     Assert all arguments have the same value
#     """
#     arguments = (arg for arg in args)
#     first = next(arguments)
#     assert all(arg == first for arg in arguments), \
#         "Not all arguments have the same value: " + str(args)
#
#
# class MultiHeadedAttention(nn.Module):
#     """
#     Multi-Head Attention module from
#     "Attention is All You Need"
#     :cite:`DBLP:journals/corr/VaswaniSPUJGKP17`.
#
#     Similar to standard `dot` attention but uses
#     multiple attention distributions simulataneously
#     to select relevant items.
#
#     .. mermaid::
#
#        graph BT
#           A[key]
#           B[value]
#           C[query]
#           O[output]
#           subgraph Attn
#             D[Attn 1]
#             E[Attn 2]
#             F[Attn N]
#           end
#           A --> D
#           C --> D
#           A --> E
#           C --> E
#           A --> F
#           C --> F
#           D --> O
#           E --> O
#           F --> O
#           B --> O
#
#     Also includes several additional tricks.
#
#     Args:
#        head_count (int): number of parallel heads
#        model_dim (int): the dimension of keys/values/queries,
#            must be divisible by head_count
#        dropout (float): dropout parameter
#     """
#
#     def __init__(self, head_count, model_dim, dropout=0.1):
#         assert model_dim % head_count == 0
#         self.dim_per_head = model_dim // head_count
#         self.model_dim = model_dim
#
#         super(MultiHeadedAttention, self).__init__()
#         self.head_count = head_count
#
#         self.linear_keys = nn.Linear(model_dim,
#                                      head_count * self.dim_per_head)
#         self.linear_values = nn.Linear(model_dim,
#                                        head_count * self.dim_per_head)
#         self.linear_query = nn.Linear(model_dim,
#                                       head_count * self.dim_per_head)
#         self.sm = nn.Softmax(dim=-1)
#         self.dropout = nn.Dropout(dropout)
#         self.final_linear = nn.Linear(model_dim, model_dim)
#
#     def forward(self, key, value, query, mask=None):
#         """
#         Compute the context vector and the attention vectors.
#
#         Args:
#            key (`FloatTensor`): set of `key_len`
#                 key vectors `[batch, key_len, dim]`
#            value (`FloatTensor`): set of `key_len`
#                 value vectors `[batch, key_len, dim]`
#            query (`FloatTensor`): set of `query_len`
#                  query vectors  `[batch, query_len, dim]`
#            mask: binary mask indicating which keys have
#                  non-zero attention `[batch, query_len, key_len]`
#         Returns:
#            (`FloatTensor`, `FloatTensor`) :
#
#            * output context vectors `[batch, query_len, dim]`
#            * one of the attention vectors `[batch, query_len, key_len]`
#         """
#
#         # CHECKS
#         batch, k_len, d = key.size()
#         batch_, k_len_, d_ = value.size()
#         aeq(batch, batch_)
#         aeq(k_len, k_len_)
#         aeq(d, d_)
#         batch_, q_len, d_ = query.size()
#         aeq(batch, batch_)
#         aeq(d, d_)
#         aeq(self.model_dim % 8, 0)
#         if mask is not None:
#             batch_, q_len_, k_len_ = mask.size()
#             aeq(batch_, batch)
#             aeq(k_len_, k_len)
#             aeq(q_len_ == q_len)
#         # END CHECKS
#
#         batch_size = key.size(0)
#         dim_per_head = self.dim_per_head
#         head_count = self.head_count
#         key_len = key.size(1)
#         query_len = query.size(1)
#
#         def shape(x):
#             return x.view(batch_size, -1, head_count, dim_per_head) \
#                 .transpose(1, 2)
#
#         def unshape(x):
#             return x.transpose(1, 2).contiguous() \
#                     .view(batch_size, -1, head_count * dim_per_head)
#
#         # 1) Project key, value, and query.
#         key_up = shape(self.linear_keys(key))
#         value_up = shape(self.linear_values(value))
#         query_up = shape(self.linear_query(query))
#
#         # 2) Calculate and scale scores.
#         query_up = query_up / math.sqrt(dim_per_head)
#         scores = torch.matmul(query_up, key_up.transpose(2, 3))
#         import IPython; IPython.embed(); raise SystemExit
#
#
#         if mask is not None:
#             mask = mask.unsqueeze(1).expand_as(scores)
#             scores = scores.masked_fill(mask, -1e18)
#
#         # 3) Apply attention dropout and compute context vectors.
#         attn = self.sm(scores)
#         drop_attn = self.dropout(attn)
#         context = unshape(torch.matmul(drop_attn, value_up))
#
#         output = self.final_linear(context)
#         # CHECK
#         batch_, q_len_, d_ = output.size()
#         aeq(q_len, q_len_)
#         aeq(batch, batch_)
#         aeq(d, d_)
#
#         # Return one attn
#         top_attn = attn \
#             .view(batch_size, head_count,
#                   query_len, key_len)[:, 0, :, :] \
#             .contiguous()
#         # END CHECK
#         return output, top_attn

##########################################################################

class MultiHeadAttention(nn.Module):
    """Multi-head attention module, as described in 'Attention is all you need'"""
    def __init__(self, num_features=480, num_heads=2):
        super().__init__()
        assert num_features % num_heads == 0
        self.d_k = num_features // num_heads  # Assume d_v == d_k
        self.num_heads = num_heads
        self.lin_key = nn.Linear(num_features, num_features)
        self.lin_query = nn.Linear(num_features, num_features)
        self.lin_value = nn.Linear(num_features, num_features)
        self.lin_final = nn.Linear(num_features, num_features)

    @staticmethod
    def sdp_attention(query, key, value, mask=None):
        """Compute scaled dot product attention

        \mathrm{Attention}(Q, K, V) = \mathrm{softmax}(\frac{QK^T}{\sqrt{d_k}})V
        """
        d_k = query.shape[-1]
        keyt = key.transpose(-2, -1)
        scores = query @ keyt / math.sqrt(d_k)
        if mask is not None:
            scores = scores.masked_fill(mask == 0, -1e9)
        att = F.softmax(scores, dim=-1)
        mul = att @ value
        return mul, att
        # return att

    def split_heads(self, x):
        return x.reshape(x.shape[0], -1, self.num_heads, self.d_k).transpose(-1, -2)

    def merge_heads(self, x):
        return x.transpose(-1, -2).reshape(x.shape[0], -1, self.num_heads * self.d_k)

    def forward(self, query, key, value, mask=None):

        if mask is not None:
            mask.unsqueeze_(1)

        hquery = self.split_heads(self.lin_query(query))
        hkey = self.split_heads(self.lin_key(value))
        hvalue = self.split_heads(self.lin_value(value))

        hmul, hatt = self.sdp_attention(hquery, hkey, hvalue, mask=mask)

        mul = self.merge_heads(hmul)
        # hatt still contains num_heads separate heads!
        out = self.lin_final(mul)
        att = hatt[:, 0]  # Arbitrarily select head 0. This is just for debugging/fast visualization

        return out, att


class ImageEmbedding(nn.Module):
    def __init__(self, L=512):
        super().__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(1, 20, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=2),
            nn.Conv2d(20, 50, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=2)
        )
        self.embed = nn.Sequential(
            nn.Linear(50 * 4 * 4, L),
            nn.ReLU(),
        )

    def forward(self, x):
        conv_out = self.conv(x)
        conv_out_flat = conv_out.view(x.shape[0], -1)
        embedding = self.embed(conv_out_flat)  # KxL
        return embedding


# Adapted from http://nlp.seas.harvard.edu/2018/04/03/attention.html
class PositionalEncoding(nn.Module):
    def __init__(self, L=512, max_len=100):
        super().__init__()
        # Compute the positional encodings once in log space.  # TODO: Why exp(log)? Numerical issues?
        pe = torch.empty(max_len, L)
        position = torch.arange(0.0, max_len).unsqueeze(1)
        # log of maximum wavelength/2pi (see https://arxiv.org/abs/1706.03762)
        logmwl = torch.log(torch.tensor(10000.0))
        div_term = torch.exp(torch.arange(0.0, L, 2) * -(logmwl / L))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)  # Register as non-learnable param

    def forward(self, embedding):
        # Add the positional encodings for all indices of the embedding bag.
        positional_embedding = embedding + self.pe[:embedding.shape[0]]
        return positional_embedding


class Identity(nn.Module):
    def forward(self, x):
        return x


class Attention(nn.Module):
    def __init__(self, L=512, D=128):
        super().__init__()
        self.attention = nn.Sequential(
            nn.Linear(L, D),
            nn.Tanh(),  # TODO: tanh could be problematic
            nn.Linear(D, 1)
        )

    def forward(self, embedding):
        att = self.attention(embedding)
        att.squeeze_(1)
        att = F.softmax(att, dim=0)
        return att


class Classifier(nn.Module):
    def __init__(self, L=512):
        super().__init__()
        self.classifier = nn.Sequential(
            nn.Linear(L, 1),
            nn.Sigmoid()
        )

    def forward(self, att, embedding):
        att_embedding = att @ embedding  # KxL
        probs = self.classifier(att_embedding)
        probs = probs.squeeze()  # Squeeze 1x1 -> ()
        return probs


class Network(nn.Module):
    def __init__(self, L=512, D=128, n_classes=10, positional=True, att_mode='msdp'):
        super().__init__()
        self.n_classes = n_classes
        self.att_mode = att_mode
        # One shared embedding
        self.embedding = ImageEmbedding(L=L)
        if positional:
            # Positional encoding (same shape as embedding, not learnable)
            self.positional_encoding = PositionalEncoding(L=L)
        else:
            # Disable positional encoding (replace with identity)
            self.positional_encoding = Identity()
        # One separate attention network per class
        if self.att_mode == 'add':  # Additive attention
            self.attentions = nn.ModuleList([
                Attention(L=L, D=D) for _ in range(n_classes)
            ])
        elif self.att_mode == 'msdp':  # Multi-head scaled dot-product attention
            self.attentions = nn.ModuleList([
                MultiHeadAttention(8, L) for _ in range(n_classes)
            ])
        # One separate scalar classifier per class
        self.classifiers = nn.ModuleList([
            Classifier(L=L) for _ in range(n_classes)
        ])

    def forward(self, x):
        # Compute one shared embedding
        emb = self.embedding(x)
        # Add positional encoding to embedding
        pemb = self.positional_encoding(emb)
        # Compute independent per-class attention and per-class probabilities
        att = [None] * self.n_classes
        probs = [None] * self.n_classes
        # Treat each target class separately
        for c in range(self.n_classes):
            # Apply attentions separately for each target class
            if self.att_mode == 'add':
                att[c] = self.attentions[c](pemb)
            elif self.att_mode == 'msdp':
                # x = pemb[:, :400].unsqueeze(0)
                x = pemb.unsqueeze(0)
                _out, att[c] = self.attentions[c](x, x, x)
                _out.squeeze_(0)
                att[c].squeeze_(0)
            # Apply classifiers separately on resp. class attentions, on the same embedding
            probs[c] = self.classifiers[c](att[c], pemb)


        # Output type is currently Tuple[List[Tensor], List[Tensor]]
        return probs, att


# AUXILIARY METHODS
def decide_label(probs, thresh=0.5):
    with torch.no_grad():
        return probs >= thresh


def calculate_error(pred, target):
    with torch.no_grad():
        error = 1. - (pred == target).float().mean()
        return error


def calculate_loss(probs, target):
    probs = torch.clamp(probs, min=1e-5, max=1. - 1e-5)
    target = target.float()
    # Negative log likelihood
    loss = -(target * torch.log(probs) + (1. - target) * torch.log(1. - probs))
    return loss
