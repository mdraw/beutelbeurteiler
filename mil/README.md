Network-based multiple- instance multi-label classification.

Code is based on https://github.com/AMLab-Amsterdam/AttentionDeepMIL, https://arxiv.org/abs/1802.04712
by Maximilian Ilse, Jakub M. Tomczak, Max Welling.

More info coming soon.