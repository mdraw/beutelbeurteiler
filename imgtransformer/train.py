import argparse
import datetime
import os
import torch
from tensorboardX import SummaryWriter


# Very experimental: Use a MIL-style attention head on top of transformer predictions to produce interpretable bag-label-aware attention map


from model import make_model
from training import LabelSmoothing, NoamOpt, SimpleLossCompute
from training import data_gen, mnistbag_gen, subsequent_mask, run_epoch
from training import bag_labels


PRINT_SEQ2SEQ = True
# PRINT_SEQ2SEQ = False

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


def greedy_decode(model, src, src_mask, max_len, start_symbol, device=torch.device('cpu')):
    model.eval()
    src = src.to(device)
    src_mask = src_mask.to(device) if src_mask is not None else src_mask
    memory = model.encode(src, src_mask)
    ys = torch.ones(1, 1, dtype=torch.int64).fill_(start_symbol).to(device)
    for i in range(max_len - 1):
        out = model.decode(
            memory,
            src_mask,
            ys,
            subsequent_mask(ys.shape[1]).to(device)
        )
        prob = model.generator(out[:, -1])
        _, next_word = torch.max(prob, dim=1)
        next_word = next_word.item()
        ys = torch.cat([
                ys,
                torch.ones(1, 1, dtype=torch.int64).fill_(next_word).to(device)
            ], dim=1
        )
    return ys


def demo_decode(model, dgen, V, use_mnist=True, ntestbatches=4, device=torch.device('cpu')):
    """Print some demos of """
    model.eval()
    if use_mnist:
        loader = iter(dgen(V, batch_size=1, epoch_size=ntestbatches))
        for i in range(ntestbatches):
            batch = next(loader)
            src = batch.src
            trg_y = batch.trg_y
            dec = greedy_decode(model, src, None, max_len=14, start_symbol=0, device=device)
            # Postprocessing: Remove start symbol from pred, upredicted end symbol from target
            # TODO: Where did the trg_y[:, -1] prediction go? max_len?
            dec = dec[:, 1:]
            trg_y = trg_y[:, :-1]
            if PRINT_SEQ2SEQ:
                print('\n== seq2seq view ==')
                print(f'True: {trg_y}')
                print(f'Pred: {dec}')
                print('\n== seq2baglabel view ==')
            true_labels = bag_labels(trg_y)
            pred_labels = bag_labels(dec)
            test_results = f'True Bag Labels, Predicted Bag Labels:\n{true_labels}\n{pred_labels}\n'
            print(test_results)
    else:
        src = torch.tensor([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]], device=device)
        trg_y = src.clone()  # Unmodified sequence copy task
        src_mask = torch.ones(1, 1, 10, device=device)
        dec = greedy_decode(model, src, src_mask, max_len=src.shape[1], start_symbol=1, device=device)
        print('\n-- Simple ascending sequence --')
        print(f'Src:  {src}')
        print(f'True: {trg_y}')
        print(f'Pred: {dec}')
        print()

        print('\n-- Random test sequences --')
        loader = iter(dgen(V, batch_size=1, epoch_size=ntestbatches))
        for i in range(ntestbatches):
            batch = next(loader)
            src = batch.src
            trg_y = batch.trg_y
            dec = greedy_decode(model, src, None, max_len=src.shape[1], start_symbol=src[0, 1], device=device)
            # trg_y = trg_y[:, :-1]
            # Remove start symbol from src and dec
            src = src[:, 1:]
            dec = dec[:, 1:]

            print(f'Src:  {src}')
            print(f'True: {trg_y}')
            print(f'Pred: {dec}')
            print()



if __name__ == '__main__':

    torch.manual_seed(1)

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--name', default='', help='Experiment name (for tensorboard)')
    parser.add_argument(
        '-s', '--saveroot', default=None,
        help='Root directory for storing experiments'
    )
    parser.add_argument(
        '-l', '--load', default=None,
        help='Load trained model from a given file path (*.pt) and skip training'
    )
    parser.add_argument(
        '-r', '--resume', default=None,
        help='Load trained model from a given file path (*.pt) and resume training'
    )
    parser.add_argument(
        '--scalars', action='store_true',
        help='Use scalars instead of MNIST images. Disable convolutional embedding.'
    )
    parser.add_argument(
        '-e', '--pretrained-embeddings', default=None,
        help='Path to pretrained image embeddings, used if in MNIST mode.'
    )
    args = parser.parse_args()
    expname = args.name

    use_mnist = not args.scalars
    if use_mnist:
        dgen = mnistbag_gen
        V = 11
    else:
        dgen = data_gen
        V = 11

    if device.type == 'cuda':
        batch_size = 30
        epoch_size = 20
        num_epochs = 20
    else:
        batch_size = 30
        epoch_size = 10
        num_epochs = 2

    saveroot = '~/imgtf' if args.saveroot is None else args.saveroot
    saveroot = os.path.expanduser(saveroot)
    timestamp = datetime.datetime.now().strftime('%H-%M-%S')
    savedir = os.path.join(saveroot, f'{expname}_{timestamp}')
    writer = SummaryWriter(savedir)
    criterion = LabelSmoothing(size=V, padding_idx=0, smoothing=0.01).to(device)
    model = make_model(V, V, N=1, img_mode=use_mnist).to(device)
    if args.pretrained_embeddings is not None:
        pretrained_embedding_path = os.path.expanduser(args.pretrained_embeddings)
        # pretrained_embedding_path = os.path.expanduser('~/data/torch/imgemb512.pth')
        if use_mnist and os.path.exists(pretrained_embedding_path):
            model.src_embed[0].load_state_dict(torch.load(pretrained_embedding_path, map_location=device))
        else:
            print('Not in MNIST mode or embedding path not found.')

    if args.resume is not None:
        model = torch.load(os.path.expanduser(args.resume), map_location=device)
    elif args.load is not None:
        model = torch.load(os.path.expanduser(args.load), map_location=device)
        num_epochs = 0  # Don't train

    model_opt = NoamOpt(
        model_size=model.src_embed[0].d_model,
        factor=1,
        warmup=400,
        optimizer=torch.optim.Adam(
            model.parameters(), lr=0, betas=(0.9, 0.98), eps=1e-9
        )
    )

    for epoch in range(num_epochs):
        model.train()
        train_stats = run_epoch(
            dgen(V, batch_size=batch_size, epoch_size=epoch_size, train=True, device=device),
            model,
            SimpleLossCompute(model.generator, criterion, model_opt, err_mode='bag'),
            writer=writer,
            epoch=epoch,
            train=True,
            use_mnist=use_mnist
        )
        model.eval()
        test_stats = run_epoch(
            dgen(V, batch_size=batch_size, epoch_size=1, train=False, device=device),
            model,
            SimpleLossCompute(model.generator, criterion, None, err_mode='bag'),
            writer=writer,
            epoch=epoch,
            train=False,
            use_mnist=use_mnist
        )
        # TODO: Make readable
        print(f' = Train stats =\n{train_stats}\n')
        print(f' = Test stats = \n{test_stats}\n')
        # print(f'Train loss: {train_stats.loss}')
        # print(f' Test loss: {test_stats.loss}')
        # writer.add_scalar('train_loss', train_loss, epoch)
        # writer.add_scalar('test_loss', test_loss, epoch)
        writer.add_scalars(
            'loss',
            {'train': train_stats.loss, 'test': test_stats.loss},
            epoch
        )

        # print(f'Train error: {train_stats.seq_error}%')
        # print(f' Test error: {test_stats.seq_error}%')
        # writer.add_scalar('train_error', train_error, epoch)
        # writer.add_scalar('test_error', test_error, epoch)
        writer.add_scalars(
            'seq_error',
            {'train': train_stats.seq_error, 'test': test_stats.seq_error},
            epoch
        )
        writer.add_scalars(
            'bag_error',
            {'train': train_stats.bag_error, 'test': test_stats.bag_error},
            epoch
        )

        demo_decode(model=model, dgen=dgen, V=V, use_mnist=use_mnist, device=device)

    torch.save(model, os.path.join(savedir, 'model.pt'))



