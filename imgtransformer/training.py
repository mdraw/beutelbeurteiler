import time
from functools import lru_cache
from dataclasses import dataclass

import numpy as np
import torch
from torch.utils import data
from torch import nn

from model import subsequent_mask
from mnistbags import MnistBags

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()  # Set seaborn defaults for plotting


def visualize_bag_imgs(bag, labels=None):
    fig, axes = plt.subplots(3, 5)#,figsize=(10, 6))
    if labels is None:
        labels = [f'{k}' for k in range(bag.shape[0])]
    bag = bag.detach().squeeze()  # (N, K, C, H, W) -> (K, H, W)
    if bag.shape[0] > 15:
        bag = bag[:15]
        print('Warning: This bag has been shortened to 15 elements for plotting.')
    for k in range(bag.shape[0], 15):
        fig.delaxes(axes.flat[k])
    for k, (img, lab) in enumerate(zip(bag, labels)):
        ax = axes.flat[k]
        ax.imshow(img, cmap='gray')
        ax.set_axis_off()
        ax.set_title(lab)
    return fig


target_classes = list(range(10))


def visualize_attention_grid(attention, target_classes=target_classes):
    fig, ax = plt.subplots()
    attperc = (attention * 100).to(torch.uint8)
    sns.heatmap(attperc, vmin=0, vmax=100, annot=True, cmap='viridis', linewidths=.5, ax=ax)
    ax.set_title('Scaled attention weights (in %)')
    ax.set_yticklabels(target_classes)
    ax.set_xlabel('Index of instance in bag')
    ax.set_ylabel('Class')
    return fig


def visualize_probs(probs):
    fig, ax = plt.subplots()
    probsperc = (probs * 100).to(torch.uint8)
    sns.heatmap(probsperc.unsqueeze(1), vmin=0, vmax=100, cmap='viridis', annot=True, linewidths=.5)
    ax.set_title('Predicted bag label probabilities (in %)')
    ax.set_yticklabels(target_classes)
    ax.set_ylabel('Class')
    return fig


class Batch:
    "Object for holding a batch of data with mask during training."
    def __init__(self, src, trg=None, pad=0, disable_src_mask=True, device=torch.device('cpu')):
        self.src = src.to(device)
        if disable_src_mask:
            self.src_mask = None
        else:
            self.src_mask = (src != pad).unsqueeze(-2).to(device)  # wonky
        if trg is not None:
            self.trg = trg[:, :-1].to(device)
            self.trg_y = trg[:, 1:].to(device)
            self.trg_mask = self.make_std_mask(self.trg, pad).to(device)
            self.ntokens = (self.trg_y != pad).data.sum(dtype=torch.float32).item()

    @staticmethod
    def make_std_mask(tgt, pad):
        "Create a mask to hide padding and future words."
        tgt_mask = (tgt != pad).unsqueeze(-2)
        tgt_mask = tgt_mask & subsequent_mask(tgt.size(-1)).type_as(tgt_mask.data)
        return tgt_mask


def run_epoch(data_iter, model, loss_compute, writer=None, epoch=0, train=True, use_mnist=False):
    "Standard Training and Logging Function"
    total_tokens = 0
    total_loss = 0
    total_seq_error = 0
    total_bag_error = 0
    for i, batch in enumerate(data_iter):
        out = model.forward(
            batch.src, batch.trg,
            batch.src_mask, batch.trg_mask
        )
        _stats = loss_compute(out, batch.trg_y, batch.ntokens)
        total_loss += _stats.loss
        total_seq_error += _stats.seq_error * 100  # In %
        total_bag_error += _stats.bag_error * 100  # In %
        total_tokens += batch.ntokens
    if writer is not None:  # Visualize last example from this epoch
        prefix = 'train_' if train else 'test_'
        if use_mnist:
            fig_bag_imgs = visualize_bag_imgs(batch.src[0])
            # fig_probs = visualize_probs(out)  # probs are not available rn
            writer.add_figure(f'{prefix}bag/images', fig_bag_imgs, epoch)
        for h in range(model.decoder.layers[0].self_attn.h):  # Different heads
            att = model.decoder.layers[0].self_attn.attn[0, h].detach().cpu()
            fig_attention = visualize_attention_grid(att)
            writer.add_figure(f'{prefix}bag/attention_h{h}', fig_attention, epoch)
        # writer.add_figure('bag/probs', fig_probs, epoch)
    total_loss /= total_tokens
    total_bag_error /= total_tokens
    total_seq_error /= total_tokens
    return Stats(loss=total_loss, seq_error=total_seq_error, bag_error=total_bag_error)


class NoamOpt:
    "Optim wrapper that implements rate."

    def __init__(self, model_size, factor, warmup, optimizer):
        self.optimizer = optimizer
        self._step = 0
        self.warmup = warmup
        self.factor = factor
        self.model_size = model_size
        self._rate = 0

    def step(self):
        "Update parameters and rate"
        self._step += 1
        rate = self.rate()
        for p in self.optimizer.param_groups:
            p['lr'] = rate
        self._rate = rate
        self.optimizer.step()

    def rate(self, step=None):
        "Implement `lrate` above"
        if step is None:
            step = self._step
        return self.factor * (self.model_size ** (-0.5) *
                              min(step ** (-0.5), step * self.warmup ** (-1.5)))


class LabelSmoothing(nn.Module):
    "Implement label smoothing."

    def __init__(self, size, padding_idx, smoothing=0.0):
        super(LabelSmoothing, self).__init__()
        try:
            self.criterion = nn.KLDivLoss(reduction='sum')
            # self.criterion = nn.KLDivLoss()
        except TypeError:  # torch<0.4.1
            self.criterion = nn.KLDivLoss(size_average=False)
        self.padding_idx = padding_idx
        self.confidence = 1.0 - smoothing
        self.smoothing = smoothing
        self.size = size
        self.true_dist = None

    def forward(self, x, target):
        assert x.size(1) == self.size
        true_dist = x.data.clone()
        true_dist.fill_(self.smoothing / (self.size - 2))
        true_dist.scatter_(1, target.data.unsqueeze(1), self.confidence)
        true_dist[:, self.padding_idx] = 0
        mask = torch.nonzero(target.data == self.padding_idx)
        if mask.dim() > 0:
            if mask.numel() > 0:
                true_dist.index_fill_(0, mask.squeeze(), 0.0)
            else:
                pass  # Can't index_fill with empty index list.

        self.true_dist = true_dist
        loss = self.criterion(x, true_dist)
        # loss = -(true_dist * torch.log(x) + (1. - true_dist) * torch.log(1. - x))
        # loss = torch.sum(torch.sum(-true_dist * torch.log_softmax(input, 1), dim=1))
        return loss


def data_gen(V, batch_size, epoch_size, train=True, device=torch.device('cpu')):
    "Generate random data for a src-tgt copy task."
    for i in range(epoch_size):
        data = torch.from_numpy(np.random.randint(1, V, size=(batch_size, 10)))
        # for i in range(data.shape[0]):
        #     data[i] = torch.arange(0, 10, dtype=torch.int64, device=device)
        data[:, 0] = _scalar_start_symbol(batch_size)  # TODO: Cat, don't overwrite
        src = data
        tgt = data
        yield Batch(src, tgt, 0, device=device)


@lru_cache(maxsize=2)
def _mnistbag_start_symbol(batch_size):
    """Get a constant start symbol that is prepended to input sequences.

    Outputs are in shape (N, 1, C, H, W) and (N, 1), so they can be
    torch.catted with an actual batch that has its sequence dimension in
    axis 1.

    Currently this is an all-zero tensor with class 10, which is ignored by loss"""
    # start_src, start_tgt = dataset.instancedataset[dataset.index_table[0][0]]
    start_src, start_tgt = torch.zeros(1, 28, 28), torch.tensor(10)
    # Replicate along batch axis. (C, H, W) -> (N, 1, C, H, W)
    start_src = start_src.repeat(batch_size, 1, 1, 1, 1)
    start_tgt = start_tgt.repeat(batch_size, 1)  # () -> (N, 1)
    return start_src, start_tgt


@lru_cache(maxsize=2)
def _scalar_start_symbol(batch_size, c=0):
    """Get a constant start symbol of class c that is prepended to input sequences.
    Class c should be a class that does not come up in actual training/test data.
    """
    start_data = torch.tensor(c).repeat(1)
    return start_data


def _worker_init_fn(worker_id):
    np.random.seed((torch.initial_seed() + worker_id) % 2**31)  # Needs to be int32
    # pass


def mnistbag_gen(V, batch_size, epoch_size, train=True, device=torch.device('cpu')):
    dataset = MnistBags(fixed_bag_length=14, num_bags=epoch_size*batch_size, train=train, order='ascending')
    start_src, start_tgt = _mnistbag_start_symbol(batch_size)
    # num_workers = 1 if device.type == 'cuda' else 0
    num_workers = 1 # Always use at least 1 because otherwise deterministic data randomness doesn't work
    loader = iter(data.DataLoader(
        dataset, batch_size=batch_size, num_workers=num_workers,
        worker_init_fn=_worker_init_fn, shuffle=train))
    for i in range(epoch_size):
        imgs, (bag_label, instance_labels) = next(loader)
        src = imgs
        tgt = instance_labels
        src = torch.cat([start_src, src], 1)
        tgt = torch.cat([start_tgt, tgt], 1)
        yield Batch(src, tgt, 0, disable_src_mask=True, device=device)


def bag_labels(seq, classes=None):
    """Treats a sequence seq as an unordered bag and returns a list of bag
    labels that indicate if a certain class is present in it."""
    if classes is None:
        classes = range(10)
    bag = set(seq.squeeze().tolist())
    present_classes = [c for c in classes if c in bag]
    return present_classes


@dataclass
class Stats:
    loss: float = np.nan
    seq_error: float = np.nan
    bag_error: float = np.nan

    def __repr__(self):
        rep = '\n'.join([f'{k}:\t{v:.4f}' for k, v in self.__dict__.items()])
        return rep


class SimpleLossCompute:
    "A simple loss compute and train function."

    def __init__(self, generator, criterion, opt=None, err_mode='seq'):
        self.generator = generator
        self.criterion = criterion
        self.opt = opt
        self.err_mode = err_mode

    def __call__(self, x, y, norm):
        x = self.generator(x)
        loss = self.criterion(
            x.contiguous().view(-1, x.shape[-1]),
            y.contiguous().view(-1)
        ) / norm
        loss.backward()
        with torch.no_grad():
            pred = x.argmax(2)
            # if self.err_mode == 'seq':
            seq_error = (pred != y).sum(dtype=torch.float32) / y.numel()
            # elif self.err_mode == 'bag':
            bag_error = 0
            # TODO: Ignore some user-defined classes (non-target classes)
            for y_, x_ in zip(y, pred):  # Iterate over batch dimensions
                true_labels = set(bag_labels(y_))
                pred_labels = set(bag_labels(x_))
                # Use set differences to calculate error
                bag_error += 1 - (len(true_labels - pred_labels) / len(true_labels))
            # else:
            #     raise ValueError
        if self.opt is not None:
            self.opt.step()
            self.opt.optimizer.zero_grad()
        stats = Stats(
            loss=loss.item() * norm,
            seq_error=seq_error,
            bag_error=bag_error,
        )
        return stats
