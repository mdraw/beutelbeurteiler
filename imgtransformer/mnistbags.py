import os

import numpy as np
import torch
import torch.utils.data as data_utils
from torchvision import datasets, transforms


def k_hot_bag(label_list, target_classes):
    """ Get a k-hot encoded bag-level tensor from a bag of instance labels.

    Instance-level information is (intentionally) lost in this conversion.
    The output tensor encodes "k-th label is present in the bag" as a
    1 at index k (where 0 <= k < n_classes). If label k is not in the bag,
    there is a 0 at index k.
    E.g. if n_classes=4, the output [0, 1, 0, 1] means that the bag contains
    the labels with class indices 1 and 3, but not 0 and 2.

    Args:
        label_list: List of labels that are true for one bag.
        target_classes: List of class indices that should be considered as
            bag classification targets. Elements of label_list that are not
            in target_classes will be ignored.
    """
    n_classes = len(target_classes)
    khot = torch.zeros(n_classes, dtype=torch.uint8)
    for c in range(n_classes):
        is_in_bag = target_classes[c] in label_list
        khot[c] = int(is_in_bag)
    return khot


def reverse_dset(dataset):
    """Builds a reverse lookup table inds,
    where inds[c] contains all (img, label) tuples where label == c.
    With it you can directly query all images with a certain label."""
    inds = {c: [] for c in range(10)}
    for img, label in dataset:
        inds[label.item()].append((img, label))
    return inds


def dset_index_table(dataset):
    """Builds a reverse lookup table inds,
    where inds[i] contains all dataset indices k where dataset[k][1] == i."""
    inds = {c: [] for c in range(10)}
    for k, (_, label) in enumerate(dataset):
        inds[label.item()].append(k)
    return inds


class MnistBags(data_utils.Dataset):
    """Pytorch dataset that creates bags of MNIST images."""
    def __init__(self, target_classes=(9,), mean_bag_length=10, min_bag_length=8,
                 var_bag_length=2, max_bag_length=float('inf'), seed=1, train=True,
                 augment=False, order=None, fixed_bag_length=-1, num_bags=250):
        # self.target_classes = np.array(target_classes)
        self.mean_bag_length = mean_bag_length
        self.var_bag_length = var_bag_length
        self.max_bag_length = max_bag_length
        self.min_bag_length = min_bag_length
        self.fixed_bag_length = fixed_bag_length
        self.num_bags = num_bags
        self.train = train
        self.target_classes = target_classes

        self.order = order

        # np.random = np.random.RandomState(seed)
        np.random.seed(seed)

        self.num_in_train = 60000
        self.num_in_test = 10000

        if augment:
            self.augmentations = transforms.Compose([
                # transforms.RandomAffine(degrees=10, translate=(0.1, 0.1), scale=(0.9, 1.1)),  # Low random warping
                transforms.Pad(2, padding_mode='reflect'),  # Reflection padding to simulate parts from other images (-> messy region proposals).
                transforms.RandomCrop(28),  # Crop to original size after other transforms. Use a random image region to simulate messy region proposals
            ])
        else:
            self.augmentations = transforms.Compose([])  # Disable augmentation transforms

        self.instancedataset = datasets.MNIST(
            os.path.expanduser('~/data/torch'),
            train=self.train,
            download=True,
            transform=transforms.Compose([
                self.augmentations,
                transforms.ToTensor(),
                transforms.Normalize((0.1307,), (0.3081,))
            ])
        )
        self.img_shape = self.instancedataset[0][0].shape

        self.index_table = dset_index_table(self.instancedataset)

    def __len__(self):
        return self.num_bags  # Temporary hardcoded value. This was effectively 250 before because of num_bag=250
        # return len(self.instancedataset) // self.mean_bag_length

    def __getitem__(self, index):
        bag, label = self.get(index)
        return bag, label

    def get_instance_indices(self, bag_length):
        random_instance_indices = list(np.random.randint(0, len(self.instancedataset), bag_length))
        if self.order is None:
            return random_instance_indices
        elif self.order == 'ascending':
            random_instance_indices.sort(key=lambda k: self.instancedataset[k][1])
            return random_instance_indices
        elif self.order == 'brace':
            # Create a brace-like internal sequence structure of the bag by
            #  enclosing c2 elements in a c1 brace. Read instances of class
            #  c1 as a brace, meaning every c1 occurence has a "partner" that exists
            #  on the other side of the c2 run in the middle.
            c1 = 4
            c2 = 8
            numpad = 2  # Left-pad with random instances
            numleft = numright = 2  # c1 elements
            nummiddle = bag_length - numleft - numright - numpad  # c2 elements in the middle, enclosed by the c1 "brace"

            pad_indices = list(np.random.choice(random_instance_indices, numpad))
            left_indices = list(np.random.choice(self.index_table[c1], numleft))
            middle_indices = list(np.random.choice(self.index_table[c2], nummiddle))
            right_indices = list(np.random.choice(self.index_table[c1], numright ))
            assembled_indices = pad_indices + left_indices + middle_indices + right_indices
            return assembled_indices

    def get(self, index):
        bag_length = np.int(np.random.normal(self.mean_bag_length, self.var_bag_length, 1))
        if bag_length < self.min_bag_length:
            bag_length = self.min_bag_length
        if bag_length > self.max_bag_length:
            bag_length = self.max_bag_length
        if self.fixed_bag_length != -1:  # Hardcode length
            bag_length = self.fixed_bag_length

        instance_indices = self.get_instance_indices(bag_length)

        bag = torch.empty(bag_length, *self.img_shape)
        instance_level_labels = torch.empty(bag_length, dtype=torch.int64)
        for k in range(bag_length):
            # k: index in bag, idx: index in instance-level dataset
            idx = instance_indices[k]
            bag[k], instance_level_labels[k] = self.instancedataset[idx]

        bag_level_label = k_hot_bag(instance_level_labels, target_classes=self.target_classes)
        label = [bag_level_label, instance_level_labels]

        return bag, label
